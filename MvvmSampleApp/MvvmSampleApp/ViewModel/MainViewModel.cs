﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmSampleApp.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private int clickCount;
        public int ClickCount
        {
            get => clickCount;
            set => Set(() => ClickCount, ref clickCount, value);
        }

        public RelayCommand IncrementCommand { get; }

        public MainViewModel()
        {
            IncrementCommand = new RelayCommand(() => ClickCount++);
        }
    }
}
