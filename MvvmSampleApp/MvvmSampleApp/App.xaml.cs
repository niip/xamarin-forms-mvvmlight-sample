﻿using MvvmSampleApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace MvvmSampleApp
{
    public partial class App : Application
    {
        private static ViewModelLocator locator;
        public static ViewModelLocator Locator
        {
            get => locator ?? (locator = new ViewModelLocator());
        }

        public App()
        {
            InitializeComponent();

            MainPage = new MvvmSampleApp.MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
