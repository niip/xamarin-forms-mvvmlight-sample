### MvvmSampleApp

When creating a Xamarin.Forms application we added the MvvmLight  
package from NuGet and found that it broke UI rendering on Android.  
  
This repository was created as a means to test the theory that the
issue was caused by the scaffolding performed by that NuGet package.  

Turns out that using the MvvmLightLibs package instead and manually  
creating the ViewModelLocator and ViewModels solves the issue